The Commerce Authorize.Net Accept Payment Methods module provides one additional
payment method to Drupal Commerce for users of the Authorize.Net payment
processing service. This method is ideal for users who wish to avoid additional
PCI validation responsibilities for their Drupal server, as it sends credit card
information directly to Authorize.Net and never reports it to Drupal. It is also
useful for those whose Drupal hosting arrangement makes it difficult to secure
their site via SSL.

The [Accept Hosted method](https://developer.authorize.net/api/reference/features/accept_hosted.html)
works much like the familiar PayPal Website Payments Standard service.  Upon
checkout, Commerce displays a secure form hosted by Authorize.net which collects
credit card data. Successful payments are reported by to the Drupal site,
without sensitive details, to allow the order to complete normally.

##Configuration
###Drupal configuration
This payment module may be added and configured via the normal Rules interface.

In order to configure your Authorize.net account in Drupal you will need the
following information from your Authorize.net account:
* API Login ID
* Transaction key
* Signature key

###Authorize.Net account settings
Accept Hosted uses the authorize-and-capture transaction mode, which fully
automates payment handling. For proper operation of this you need to configure a
webhook in your Authorize.net account. The webhook should be configured to
listen to all events and on a URL like
`https://your.site/authnet_accept/webhook`.

You also need to enable the **Transaction Details API**.

##Considerations
###JavaScript

The payment gateway will function correctly with JavaScript disabled in the
browser.

###SSL

From a technical standpoint, this payment gateway does not require SSL on the
Drupal site in order to be fully secure. From a practical standpoint, users
expect SSL during a payment process and in order to use a webhook you need SSL.

##Troubleshooting

Please note that transactions cannot be processed normally when the Drupal site
is off-line.

##Credits

This module is heavily based on [Commerce Authorize.Net SIM/DPM Payment Methods](https://www.drupal.org/project/commerce_authnet_simdpm)
